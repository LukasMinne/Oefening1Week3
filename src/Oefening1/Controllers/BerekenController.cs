﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Oefening1.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Oefening1.Controllers
{
    public class BerekenController : Controller
    {
        // GET: /<controller>/
        public IActionResult Faculteit(string waarde)
        {
            if (!string.IsNullOrEmpty(waarde))
            {
                //waarde meegegeven
                int getal = Int32.Parse(waarde);
                Faculteit fac = new Faculteit(getal);
                return View();
            }
            return View();
        }
    }
}
