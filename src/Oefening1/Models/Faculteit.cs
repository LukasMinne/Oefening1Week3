﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Oefening1.Models
{
    public class Faculteit
    {
        public Faculteit(int getal)
        {
            this.Getal = getal;
        }

        public int Getal { get; set; }

        public int Bereken()
        {
            int res = 1;
            while (Getal > 0)
            {
                res = res * Getal;
                Getal--;
            }
            return res;
        }
    }
}
